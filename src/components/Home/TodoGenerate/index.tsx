import { Button } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import React, { FunctionComponent, useState } from 'react'

export interface TodoGenerateInterface {
  onPress?: Function,
}

const TodoGenerate: FunctionComponent<TodoGenerateInterface> = ({ onPress }: TodoGenerateInterface) => {
  const [input, setInput] = useState('');

  return (
    <>
      <TextField 
        onChange={(event) => {setInput(event.target.value);}} 
        id="standard-basic" label="Standard" />
      
      {onPress &&
        <Button variant="contained" color="primary" 
        onClick={() => onPress(input)}>
          Primary
        </Button>}

    </>)
}

export default TodoGenerate