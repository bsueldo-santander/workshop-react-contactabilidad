import { Button, TextField } from '@material-ui/core'
import React, { FunctionComponent, useState, useEffect } from 'react'
import styled from 'styled-components'

const WrappTodoList = styled.div`
    margin-top: 50px;

    .todo-list {
        display: flex;
        justify-content: center;
    }
    
`

export interface TodoListInterface {
    values?: string[],
}

const TodoList: React.FC<TodoListInterface> = ({ values }: TodoListInterface) => {

    const [list, setList] = useState<string[]>([])

    useEffect(() => {
        values && values.length > 0 && setList(values);
    }, [])


    return (
        <WrappTodoList>
            <ul>
                {list && list.length > 0 &&
                    list.map(x => {
                        return (<li>{x}</li>)
                    })}
            </ul>
        </WrappTodoList>)
}

export default TodoList