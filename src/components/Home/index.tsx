import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import TodoList from './components/TodoList'
import TodoGenerate from './TodoGenerate'

const WrappHome = styled.div`
    margin-top: 50px;
`

const Home = () => {
  const [todoList, setTodoList] = useState<string[]>([]);

  const agregateTodo = (todo: string) => {

    setTodoList([...todoList, todo]);

    console.log(todoList);
  }

  useEffect(() => {
    setTodoList(['Primero', 'Segundo']);
    
  }, [])

  return <WrappHome>
    <TodoGenerate onPress={ agregateTodo }></TodoGenerate>
    <br />
    {
      todoList && todoList.length > 0 && <TodoList values={ todoList }></TodoList>
    }
  </WrappHome>
}

export default Home;