import React, { FunctionComponent } from 'react'
import { Link } from 'react-router-dom'
import './index.css'
const Header: FunctionComponent = () => {
    return (
        <div className='header'>
            <span>
                <Link to="/">
                    Home
                </Link>
            </span>
               /   
            <span>
                <Link to="/about">
                    About
                </Link>
            </span>
        </div>


    )
}

export default Header;