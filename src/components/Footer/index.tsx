import React, { FunctionComponent } from 'react'
import styled from 'styled-components';

const WrappFooter = styled.div`
    height: 40px;
    width: 100%;
    background-color: grey;
    opacity: 0.7;
    font-size: 1.2rem;
    color: black;
    font-weight: 600;
    display: flex;
    position: fixed;
    bottom: 0;
    left: 0;
    display: flex;
    justify-content: center;
`

const Footer: FunctionComponent = () => {
    return (
        <WrappFooter>
            <span className='span-pepito'>hola soy un footer</span>
        </WrappFooter>

    )
}

export default Footer;