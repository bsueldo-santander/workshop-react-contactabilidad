import { Route, Switch } from 'react-router-dom'
import React, { FunctionComponent } from 'react'
import styled from 'styled-components'
import About from '../About'
import Home from '../Home'

const WrappMain = styled.div`
    margin-top: 50px;
`

const Main = () => {
    return (
  <WrappMain>
      <Switch>
        <Route path="/about">
            <About/>
        </Route>
        <Route path="/">
            <Home/>
        </Route>
      </Switch>
  </WrappMain>)
}

export default Main