import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header'
import Footer from './components/Footer';
import Main from './components/Main'
import { BrowserRouter as Router, Route} from 'react-router-dom'
function App() {
  return (
    <Router>
      <Header></Header>
      <Main></Main>
      <Footer></Footer>
    </Router>
  );
}



export default App;
